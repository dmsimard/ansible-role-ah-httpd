# {{ ansible_managed }}

{% if document_root is defined %}
<Directory "{{ document_root }}">
    Require all granted
    AllowOverride All
</Directory>

DocumentRoot {{ document_root }}
{% endif %}

{% if redirect is defined %}
RewriteEngine on
# for lets encrypt automation
RewriteCond %{REQUEST_URI} !^/.well-known/acme-challenge/
RewriteRule ^/(.*)         {{ redirect }}$1 [L,R]
{% endif %}

{% for redirect in redirects %}
{% if redirect.match|default(False) %}
RedirectMatch {{ redirect.src }} {{ redirect.target }}
{% else %}
Redirect {{ redirect.src }} {{ redirect.target }}
{% endif %}
{% endfor %}

# security measures
{% if content_security_policy is defined %}
Header always set Content-Security-Policy "{{ content_security_policy }}"
{% endif %}
{% if x_frame_options != '' %}
Header always set X-Frame-Options {{ x_frame_options }}
{% endif %}

{% for alias in aliases %}
Alias "{{ alias.url }}" "{{ alias.path }}"
<Directory "{{ alias.path }}">
    Require all granted
</Directory>
{% endfor %}

{% if website_user is defined %}
<Location />
    AuthType Basic
    AuthName "Restricted access, contact OSAS for password"
    AuthUserFile {{ httpd_confdir }}/{{ _website_domain }}.htpasswd
    Require valid-user
</Location>
{% endif %}

{% if use_mod_speling %}
{% if ansible_os_family == "RedHat" %}
LoadModule speling_module modules/mod_speling.so
{% endif %}
CheckSpelling on
{% endif %}

{% if use_mod_sec %}
SecRuleEngine {{ 'DetectionOnly' if mod_sec_detection_only else 'On' }}
{% endif %}

